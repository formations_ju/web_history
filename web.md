---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "www.png"
slideNumber: true
title: "VSCode Reveal intro"
---

## Qu'est-ce que le web?

---

## Web VS Internet

Est-ce que le web et internet c'est la même chose?

Est-ce qu'on a besoin du web pour aller sur internet?

ou l'inverse ?

--

## Internet

C'est l'ensemble des protocoles et methodes permettant à des applications de communiquer.

Quelques protocoles:
- TCP
- IP
- DNS
- ...

On peut faire de l'internet sans faire du web.

Internet est né dans les universités américaines a début des années 70.

--

## Web

C'est l'ensemble des pages Web accessible via un navigateur Web.

On ne pas faire des Web sans Internet. D'ailleurs le Web et le procotole associé(HTTP) sont nés au début des années 90.

---

## Interlude Réseau

modèle OSI & TCP/IP:

<img width="800" data-src="modele-osi-vs-tcp.png" alt="modele OSI">


---

## Les protocoles
### et plus spécifiquement le HTTP

Pour communiquer deux applications utilisent tout un tas de protocole: IP, TCP, HTTP, DHCP [...](https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)

Nous allons nous concentrer plus spécifiquement sur le protocole HTTP, car il est la base de la navigation sur le WEB

--

## Le HTTP

le protocole HTTP se compose de 4 partie:

- la méthode(GET, POST, ...)
- l'url(www.google.com)
- les headers
- le body

<small>HTTP/HTTPS: la partie sécurité sera géré par le navigateur et le serveur.</small>

---

## Qui parle avec qui ??

Un serveur et un navigateur

le navigateur est l'application cliente qui pose les questions.

Le serveur est l'application fournisseuse qui ecoute et répond aux questions.

--

### Qu'est-ce qu'on demande et qu'est-ce qu'on reçoit??

HTTP(url) -> fichier physique sur un serveur.

Navigateur(requete HTTP) -> fichier HTML.

--

## Petit schema

<img width="800" data-src="serveurWeb1.png" alt="server Web">

--

## 1ere Evolution

CSS -> Cascading Style Sheet